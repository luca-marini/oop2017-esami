package a04.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class TurnamentImpl implements Turnament {

	private List<Player> players = new ArrayList<>();
	private List<Match> matches = new ArrayList<>();
	private List<Player> winners = new ArrayList<>();
	private boolean start =false;
	private Map<Player, Set<Player>> opponents = new HashMap<Player, Set<Player>>();

	@Override
	public Player makePlayer(int id, String name) {
		return new PlayerImpl(id, name);
	}

	@Override
	public Match makeMatch(Player p1, Player p2) {
		this.opponents.get(p1).add(p2);
		this.opponents.get(p2).add(p1);
		return new MatchImpl(p1,p2);
	}

	@Override
	public void registerPlayer(Player player) {
		this.players.add(player);
	}
	
	private void matchPlayers(List<Player> p) {
		for(int i = 0; i < p.size(); i +=2) {
			this.matches.add(new MatchImpl(p.get(i), p.get(i + 1)));
		}
	}

	@Override
	public void startTurnament() {
		if(getPlayers().size() == 2 || getPlayers().size() == 4 || getPlayers().size() == 8 
				|| getPlayers().size() == 16 || getPlayers().size() == 32 || getPlayers().size() == 64
				|| getPlayers().size() == 128) {
			for(int i = 0; i < players.size(); i++) {
				opponents.put(players.get(i), new HashSet<Player>());
				winners.add(players.get(i));
			}
			this.start = true;
			this.matchPlayers(players);
		}
	}

	@Override
	public List<Player> getPlayers() {
		return this.players;
	}

	@Override
	public List<Match> getPendingGames() {
		if(!isTurnamentOver()) {
			if(this.matches.isEmpty()) {
				this.matchPlayers(winners);
			}
		}
		return this.matches;
	}

	Player getLoser(Match match, Player winner) {
		return match.getFirstPlayer() == winner ? match.getSecondPlayer() : match.getFirstPlayer();
	}
	
	@Override
	public void playMatch(Match match, Player winner) {
		if(!isTurnamentOver()) {
			this.matches.remove(match);
			this.winners.remove(getLoser(match, winner));
			
		}
	}

	@Override
	public boolean isTurnamentOver() {
		if(this.winners.size() == 1) {
			this.start = false;
		}
		return !this.start;
	}

	@Override
	public Player winner() {
		if(this.winners.size() == 1) {
			return this.winners.get(0);
		}
		return null;
	}

	@Override
	public Set<Player> opponents(Player player) {
		return this.opponents.get(player);
	}
	


}
