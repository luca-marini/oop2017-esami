package a04.e2;

public interface Strategy {

	static enum Result{
		NOTHING,THREE,FOUR,FULL,YAHTZEE,STRAIGHT
	}
	void hit(int pos);
	
	boolean isEnabled(int pos);
	
	void changeValue();
	
	int getValue(int pos);
	
	Result checkResult();
}
