package a04.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import a04.sol2.Logics.Result;

public class StrategyImpl implements Strategy {

	private final int MAXVALUE = 6;
	private final int MINVALUE = 1;
	
	private final int size;
	private List<Integer> val;
//	private List<Boolean> change;
	private Random rn = new Random();
	
	public int getValue(int pos) {
		return this.val.get(pos);
	}
	
	
	public StrategyImpl(int size) {
		this.size = size;
		this.val = Stream.generate(() -> rn.nextInt(MAXVALUE) + MINVALUE).limit(5).collect(Collectors.toCollection(ArrayList::new));
//		this.change = new ArrayList<Boolean>();
//		for(int i = 0; i < size; i++) {
//			val.add(rn.nextInt(this.MAXVALUE) + this.MINVALUE);
//			change.add(true);
		}
		
	}

	@Override
	public void hit(int pos) {
		this.change.set(pos, false);
	}

	@Override
	public boolean isEnabled(int pos) {
		return this.change.get(pos);
	}

	@Override
	public void changeValue() {
		for(int i = 0; i < size; i++) {
			if(!this.isEnabled(i)) {
				val.set(i, rn.nextInt(this.MAXVALUE) + this.MINVALUE);
			}
			this.change.set(i, true);
		}
	}

	@Override
	public Result checkResult() {
		Map<Integer,Integer> map = this.val.stream().collect(Collectors.toMap(x->x,x->1,(x,y)->x+y));
		if (map.size() == 5 && (!map.containsKey(1) || !map.containsKey(6))) { 
			return Result.STRAIGHT;
		} else if (map.size() == 3 && map.containsValue(3)) {
			return Result.THREE;
		} else if (map.size() == 2 && map.containsValue(3)) {
			return Result.FULL;
		} else if (map.size() == 2) {
			return Result.FOUR;
		} else if (map.size() == 1) {
			return Result.YAHTZEE;
		} else {
			return Result.NOTHING;
		}
	}

}
