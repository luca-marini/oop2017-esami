package a04.e2;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;


public class GUI extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8976182355465144523L;
	private final int size = 5;
	Strategy s;
	
	private void setStrategy(Strategy s) {
		this.s = s;
	}
	
	public GUI() {
		
		this.setStrategy(new StrategyImpl(size));
		
		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		List<JButton> buttons = new ArrayList<>();
		
		for(int i = 0; i < size; i++) {
			JButton b = new JButton(String.valueOf(this.s.getValue(i)));
			buttons.add(b);
			b.addActionListener(e -> {
				this.s.hit(buttons.indexOf(e.getSource()));
				b.setEnabled(this.s.isEnabled(buttons.indexOf(e.getSource())));
			});
			this.getContentPane().add(b);
		}
		
		JButton draw = new JButton("Draw");
		draw.addActionListener(e -> {
			this.s.changeValue();
			for(int i = 0; i < size; i++) {
				buttons.get(i).setEnabled(this.s.isEnabled(i));
				buttons.get(i).setText(String.valueOf(this.s.getValue(i)));
			}
			System.out.println(this.s.checkResult());
		});
		this.getContentPane().add(draw);
		
		
		this.setVisible(true);
		
	}
	
}

