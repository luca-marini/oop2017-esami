package a04.e2;



import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;


public class Example extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7681706593267671336L;
	private Logics s;
	private final List<JButton> buttons = new ArrayList<>();
	private final int SIZE = 5;
	
	
	private void setStrategy(Logics s) {
		this.s = s;
	}
	void updateView() {
		this.buttons.stream()
					.forEach(b -> b.setText(String.valueOf(this.s.getDices().get(buttons.indexOf(b)))));	
		this.buttons.stream().forEach(b -> b.setEnabled(true));
	}
	public Example(){
	
		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		this.setStrategy(new LogicsImpl());
		for(int i = 0; i < SIZE; i++) {
			this.buttons.add(new JButton(String.valueOf(s.getDices().get(i))));
			this.getContentPane().add(buttons.get(i));
			this.buttons.get(i).addActionListener(e -> {
				((JButton)e.getSource()).setEnabled(false);
			});
		}
		
		JButton draw = new JButton("Draw");
		draw.addActionListener(e -> {
			
			this.s.redraw(this.buttons.stream()
									  .map(b -> !b.isEnabled())
									  .collect(Collectors.toList()));
			updateView();
			System.out.println(this.s.getResult());
			
		});
		this.getContentPane().add(draw);
	
		this.setVisible(true);
	}

	

}
