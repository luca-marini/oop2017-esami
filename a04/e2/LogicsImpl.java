package a04.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics {
	
	private List<Integer> dices = null;
	private final Random r = new Random();

	public LogicsImpl() {
		this.dices = Stream.generate(() -> this.drawOne()).limit(5).collect(Collectors.toCollection(ArrayList::new));
	}
	
	@Override
	public List<Integer> getDices() {
		return Collections.unmodifiableList(this.dices);
	}
	
	private int drawOne() {
		return this.r.nextInt(6) + 1;
	}

	@Override
	public void redraw(List<Boolean> l) {
		for(int i = 0; i < l.size(); i++) {
			if(l.get(i)) {
				this.dices.set(i, this.drawOne());
			}
		}
	}

	@Override
	public Result getResult() {
		Map<Integer, Integer> map = this.dices.stream().collect(Collectors.toMap(x->x, x->1, (x,y)->x+y));
		if(map.size() == 1) {
			return Result.YAHTZEE;
		} else if(map.size() == 3 && map.containsValue(3)) {
			return Result.THREE;
		} else if(map.size() == 2 && map.containsValue(4)) {
			return Result.FOUR;
		} else if(map.size() == 5 && (!map.containsKey(1) || !map.containsKey(6))) {
			return Result.STRAIGHT;
		} else if(map.size() == 2 && map.containsValue(3)) {
			return Result.FULL;
		} else {
			return Result.NOTHING;
		}
	}

}
