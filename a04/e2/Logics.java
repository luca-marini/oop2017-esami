package a04.e2;

import java.util.List;

public interface Logics {

	static enum Result {
		YAHTZEE, STRAIGHT, THREE, FOUR, NOTHING, FULL;
	}
	
	List<Integer> getDices();
	
	void redraw(List<Boolean> l);
	
	Result getResult();
	
}
