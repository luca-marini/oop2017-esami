package a01b.e2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogicsImpl implements Logics {

	private final int MAXVALUE = 99;
	private List<Integer> val = new ArrayList<>();
	private Random r = new Random();
	private int attempts = 0;
	private int min;
	
	public LogicsImpl(int size) {
		this.val = Stream.generate(() -> nextOne())
						 .limit(size)
						 .collect(Collectors.toCollection(ArrayList::new));
	}

	
	private int nextOne() {
		return r.nextInt(100);
	}
	
	@Override
	public boolean isMin(int pos) {
		this.attempts++;
		if(val.indexOf(findMin()) == pos) {
			this.min = findMin();
			this.val.set(this.val.indexOf(getMin()), MAXVALUE + 1);
			return true;
		}
		return false;
	}

	@Override
	public int getMin() {
		return this.min;
	}


	@Override
	public int getAttempt() {
		return this.attempts;
	}


	@Override
	public int findMin() {
		return this.val.stream()
				   .min((a,b) -> a-b)
				   .get().intValue();
	}

}
