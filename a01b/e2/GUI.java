package a01b.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.*;

import javax.swing.*;

public class GUI extends JFrame{
	
	private List<JButton> buttons;
	private Logics logic;
	
	private void setStrategy(Logics l) {
		this.logic = l;
	}
	
	public GUI(int size){
		
		this.setStrategy(new LogicsImpl(size));
		
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		
		ActionListener ac = e-> {
			JButton b =(JButton)e.getSource();
			if(this.logic.isMin(buttons.indexOf(b))) {
				b.setEnabled(false);
				b.setText(String.valueOf(logic.getMin()));
			}
			System.out.println(logic.getAttempt());
		};
		
		buttons = Stream.generate(() -> new JButton("*"))
						.limit(size)
						.peek(b -> b.addActionListener(ac))
						.peek(b -> this.getContentPane().add(b))
						.collect(Collectors.toCollection(ArrayList::new));
		
		
		this.setVisible(true);
	}
	
}
