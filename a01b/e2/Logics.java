package a01b.e2;

public interface Logics {

	boolean isMin(int pos);
	
	int getMin();
	
	int findMin();
	
	int getAttempt();
	
	
}
