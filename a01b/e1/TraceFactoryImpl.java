package a01b.e1;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.*;

public class TraceFactoryImpl implements TraceFactory {


	@Override
	public <X> Trace<X> fromSuppliers(Supplier<Integer> sdeltaTime, Supplier<X> svalue, int size) {
		return new Trace<X>() {

			int time = 0;
			int c = 0;
			
			@Override
			public Optional<Event<X>> nextEvent() {
				if(!iterator().hasNext()) {
					return Optional.empty();
				}
				this.time += sdeltaTime.get();
				this.c++;
				return Optional.of(new EventImpl<X>(time - sdeltaTime.get(), svalue.get()));
			}

			@Override
			public Iterator<Event<X>> iterator() {
				return new Iterator<Event<X>>() {

					@Override
					public boolean hasNext() {
						return c < size;
					}

					@Override
					public Event<X> next() {
						return nextEvent().get();
					}
				};
			}

			@Override
			public void skipAfter(int time) {
				while(this.time != time + sdeltaTime.get()) {
					if(!iterator().hasNext()){
						return ;
					}
					nextEvent();
				}				
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				return new Trace<X>() {

					@Override
					public Optional<Event<X>> nextEvent() {
						return ;
					}

					@Override
					public Iterator<Event<X>> iterator() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public void skipAfter(int time) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public Trace<X> combineWith(Trace<X> trace) {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Trace<X> dropValues(X value) {
						// TODO Auto-generated method stub
						return null;
					}
					
				};
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
	}

	@Override
	public <X> Trace<X> constant(Supplier<Integer> sdeltaTime, X value, int size) {
		return new Trace<X>(){

			int time = 0;
			int c = 0;
			@Override
			public Optional<Event<X>> nextEvent() {
				if(!iterator().hasNext()) {
					return Optional.empty();
				}
				this.time += sdeltaTime.get();
				this.c++;
				return Optional.of(new EventImpl<X>(time - sdeltaTime.get(), value));
			}

			@Override
			public Iterator<Event<X>> iterator() {
				return new Iterator<Event<X>>() {

					@Override
					public boolean hasNext() {
						return c < size;
					}

					@Override
					public Event<X> next() {
						return nextEvent().get();
					}
					
				};
			}

			@Override
			public void skipAfter(int time) {
				while(this.time != time) {
					nextEvent();
				}
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return null;
			}
			};
				
	}
	@Override
	public <X> Trace<X> discrete(Supplier<X> svalue, int size) {
		return new Trace<X>(){
			
			int time = 0;
			@Override
			public Optional<Event<X>> nextEvent() {
				if(!iterator().hasNext()) {
					return Optional.empty();
				}
				return Optional.of(new EventImpl<X>(time++, svalue.get()));
			}

			@Override
			public Iterator<Event<X>> iterator() {
				return new Iterator<Event<X>>() {

					@Override
					public boolean hasNext() {
						return time < size;
					}

					@Override
					public Event<X> next() {
						return nextEvent().get();
					}
				};
			}

			@Override
			public void skipAfter(int time) {
				while(this.time != time) {
					nextEvent();
				}
			}

			@Override
			public Trace<X> combineWith(Trace<X> trace) {
				return null;
			}

			@Override
			public Trace<X> dropValues(X value) {
				// TODO Auto-generated method stub
				return null;
			};
		};


	}
}
