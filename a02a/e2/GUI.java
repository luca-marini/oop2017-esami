package a02a.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    public GUI(int size) {
    	
    	Matrix val = new MatrixImpl(size);
    	List<JButton> buttons = new ArrayList<>();
    	
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        int cols = size;
        JPanel panel = new JPanel(new GridLayout(cols,cols));
        this.getContentPane().add(BorderLayout.CENTER,panel);

        for (int i=0;i<size*size;i++){
            final JButton b = new JButton(" ");
            buttons.add(b);
            b.addActionListener(e -> {
            	if(val.getValue(buttons.indexOf(b)) == 0) {
            	    b.setText("*");
            	} else {
            	    b.setText(" ");
            	}
            	val.setValue(buttons.indexOf(b));
            	if(val.isAllColumnsEquals(buttons.indexOf(b)) || val.isAllRowsEquals(buttons.indexOf(b))) {
            		System.exit(0);
            	}
            });
            panel.add(b);
        } 
        this.setVisible(true);
    }
    
}
