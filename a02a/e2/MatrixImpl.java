package a02a.e2;

public class MatrixImpl implements Matrix {

	private int[] val;
	private final int size;
	
	public MatrixImpl(int size) {
		this.size = size;
		this.val = new int[size*size];
	}

	@Override
	public int[] getAdiacencyColumns(int i) {
		int currCol = i % size;
		int[] adjCol = new int[size - 1];
//		if(currCol == 0) {
//			for(int k = 1; k < size - 1; k++) {
//				adjCol[k] = i+k;
//			}
//		}
//		if(currCol == 1) {
//			for(int k = 0; k < size - 1; k++) {
//				if(k == 0) {
//					adjCol[k] = i-1;
//				} else {
//					adjCol[k] = i+k;
//				}
//			}
//		}
		for(int k = 0, j = -(currCol); k < size - 1; k++, j++) {
		    if(j == 0) {
			j++;
		    }
		adjCol[k] = i+j;
		}
		return adjCol;
	}

	@Override
	public int[] getAdiacencyRows(int i) {
		int currRow = i / size;
		int[] adjRow = new int[size - 1];
		
		for(int k = 0, j = -(currRow); k < size - 1; k++, j++) {
		    if(j == 0) {
		        j++;
		    }
		    adjRow[k] = i + (j * size);
		}
//		
//		if(currRow == 0) {
//			return adjRow = new int[] {i+size,i+(size*2),i+(size*3)};
//		}
		return adjRow;
	}

	@Override
	public boolean isAllColumnsEquals(int i) {
		int[] adjCol = this.getAdiacencyColumns(i);
		
		for(int v : adjCol) {
		    if(this.getValue(v) != 1) {
			return false;
		    }
		}
		return true;
	}

	@Override
	public boolean isAllRowsEquals(int i) {
	    int[] adjRow = this.getAdiacencyRows(i);
		
	    for(int v : adjRow) {
	        if(this.getValue(v) != 1) {
	            return false;
	        }
	    }
	    return true;
	}

	@Override
	public void setValue(int i) {
	    if(this.getValue(i) == 0) {
		this.val[i] = 1;
	    } else {
		this.val[i] = 0;
	    }
	}

	@Override
	public int getValue(int i) {
	    return val[i];
	}

}
