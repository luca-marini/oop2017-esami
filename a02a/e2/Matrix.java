package a02a.e2;

public interface Matrix {

	public int[] getAdiacencyColumns(int i);
	
	public int[] getAdiacencyRows(int i);
	
	public int getValue(int i);
	
	public void setValue(int i);
	
	public boolean isAllColumnsEquals(int i);
	
	public boolean isAllRowsEquals(int i);
	
}
