package a02b.e2;

import java.util.List;

public interface Logics {

	List<String> getStrings();
	
	void removeString(int pos);
	
	void concatString(int pos);
	
	void addString(int pos, String s);
		
}
