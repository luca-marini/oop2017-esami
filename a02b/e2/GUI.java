package a02b.e2;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;
import java.awt.*;
import javax.swing.*;

public class GUI {

	
	
	private final Logics logic;
	private final BufferedWriter bw;
	public GUI(String fileName) throws IOException {
		this.logic = new LogicsImpl(fileName);
		this.bw = new BufferedWriter(new FileWriter(fileName));
		final JFrame gui = new JFrame();
		final JComboBox<String> combo = new JComboBox<>();
		final JButton remove = new JButton("Remove");
		final JButton concat = new JButton("Concat");
		final JButton add = new JButton("Add");
		combo.removeAllItems();

		for(String s : logic.getStrings()) {
			combo.addItem(s);
		}
			
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JPanel main = new JPanel();
		main.setLayout(new FlowLayout());
		main.add(combo);
		main.add(remove);
		main.add(concat);
		main.add(add);
		remove.addActionListener(e -> {
			if(!logic.getStrings().isEmpty()) {
				logic.removeString(combo.getSelectedIndex());
				combo.removeAllItems();
				for(String s : logic.getStrings()) {
					combo.addItem(s);
					System.out.println(s);
					try {
						bw.append(s);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			
		});
		concat.addActionListener(e -> {
			if(!logic.getStrings().isEmpty()) {
				logic.concatString(combo.getSelectedIndex());
				combo.removeAllItems();
				for(String s : logic.getStrings()) {
					combo.addItem(s);
					System.out.println(s);
					try {
						bw.append(s);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		add.addActionListener(e -> {
			if(!logic.getStrings().isEmpty()) {
				logic.addString(combo.getSelectedIndex(),"*" + logic.getStrings().get(combo.getSelectedIndex()));
				combo.removeAllItems();
				for(String s : logic.getStrings()) {
					combo.addItem(s);
					System.out.println(s);
					try {
						bw.append(s);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		gui.getContentPane().add(main);
		gui.pack();
		gui.setVisible(true);

	}

}