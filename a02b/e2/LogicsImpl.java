package a02b.e2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.LongStream;

public class LogicsImpl implements Logics {

	private List<String> strings = new ArrayList<String>();
	BufferedReader buff;
	BufferedReader buff2;
	Long n;

	public LogicsImpl(String fileName) throws FileNotFoundException {
		super();
		this.buff = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		this.buff2 = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
		this.n = buff2.lines().count();
		LongStream.range(0, n).forEach(i -> {
			try {
				strings.add(buff.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});;
	}

	@Override
	public List<String> getStrings() {
		return Collections.unmodifiableList(strings);
	}

	@Override
	public void removeString(int pos) {
		this.strings.remove(pos);
	}

	@Override
	public void concatString(int pos) {
		this.strings.set(pos, this.strings.get(pos) + this.strings.get(pos));
	}

	@Override
	public void addString(int pos, String s) {
		List<String> l = new ArrayList<String>();
		for(int i = 0; i <= pos ; i++) {
			l.add(strings.get(i));
		}
		l.add(s);
		for(int i = pos + 1; i < strings.size() ; i++) {
			l.add(strings.get(i));
		}
		this.strings = l;
	}

}
