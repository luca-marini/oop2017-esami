package a02b.e1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class TransducerFactoryImpl implements TransducerFactory {

	@Override
	public <X> Transducer<X, String> makeConcatenator(int inputSize) {
		return new Transducer<X, String>() {
			
			private List<X> input = new ArrayList<>();
			private boolean stopInput = false;
			private int c = 0;
			
			@Override
			public void provideNextInput(X x) {
				this.input.add(x);
			}

			@Override
			public void inputIsOver() {
				if(this.stopInput) {
					throw new IllegalStateException();
				}
				this.stopInput = true;
			}

			@Override
			public boolean isNextOutputReady() {
				if(this.stopInput == true && this.input.size() > 0 && this.c < this.input.size()) {
					return true;
				}
				return c <= this.input.size()-inputSize && this.input.size() >= inputSize;
			}

			@Override
			public String getOutputElement() {
				if(isNextOutputReady() && this.stopInput == true) {
					String s = "";
					this.c+=inputSize;
					for(int i = c - inputSize; i < this.input.size(); i++) {
						s += String.valueOf(this.input.get(i));				
					}
					return s;
				}
				
				if(isNextOutputReady()) {
					this.c+=inputSize;
					String s = "";
					for(int i = c - inputSize; i < c ; i += inputSize) {
						for(int j = i; j < i + inputSize; j ++) {
							s += String.valueOf(this.input.get(j));
						}
					}				
					return s;
				}
				throw new IllegalStateException();
			}

			@Override
			public boolean isOutputOver() {
				return this.c >= this.input.size() && this.stopInput;
			}
		};
	}

	@Override
	public Transducer<Integer, Integer> makePairSummer() {
		return new Transducer<Integer, Integer>() {
			
			private List<Integer> input = new ArrayList<Integer>();
			private boolean stopInput = false;
			
			@Override
			public void provideNextInput(Integer x) {
				this.input.add(x);
			}
			
			@Override
			public boolean isOutputOver() {
				return this.stopInput && !isNextOutputReady();
			}
			
			@Override
			public boolean isNextOutputReady() {
				if(this.stopInput == true && !this.input.isEmpty()) {
					return true;
				}
				return this.input.size() > 2;
			}
			
			@Override
			public void inputIsOver() {
				this.stopInput = true;
			}
			
			@Override
			public Integer getOutputElement() {
				if(isNextOutputReady()) {
				if(this.input.size() == 1) {
					int next = this.input.get(0);
					this.input.remove(0);
					return next;
				}
				int next = 0;
				for(int i = 0; i < 2; i++) {
					next += this.input.get(i);
				}
				this.input.remove(0);
				this.input.remove(0);
				
				return next;
				} 
				throw new IllegalStateException();
			}
		};
	}

}
