package a03a.e1;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

public class ExamsManagementImpl implements ExamsManagement {

    
    public ExamsManagementImpl(){
        
    }
    
    private final Set <Pair<Integer, String>> students = new HashSet<>();
    private final Map <Pair<Integer, String>, Pair<Integer, Integer>> exams = new TreeMap<>();
    
    @Override
    public void createStudent(int studentId, String name) {
        Pair<Integer, String> s = new Pair<>(studentId, name);
        this.students.add(s);
    }

    @Override
    public void createExam(String examName, int incrementalId) {
        Pair<Integer, String> e = new Pair<>(incrementalId, examName);
        exams.putIfAbsent(e, null);
      
    }

    @Override
    public void registerStudent(String examName, int studentId) {
        exams.entrySet().stream()
                        .filter(p -> p.getKey()
                        .getY()
                        .equals(examName))
                        .map(p -> p.setValue(new Pair<>(studentId, null)));
    }

    @Override
    public void examStarted(String examName) {
        

    }

    @Override
    public void registerEvaluation(int studentId, int evaluation) {
        exams.entrySet().stream()
                        .filter(p -> p)
    }

    @Override
    public void examFinished() {
        // TODO Auto-generated method stub

    }

    @Override
    public Set<Integer> examList(String examName) {
        return this.exams.entrySet().stream()
                                    .filter(p -> p.getKey().getY().equals(examName))
                                    .map(p -> p.getValue()).collect(new Set <Integer>());
    }

    @Override
    public Optional<Integer> lastEvaluation(int studentId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, Integer> examStudentToEvaluation(String examName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<Integer, Integer> examEvaluationToCount(String examName) {
        // TODO Auto-generated method stub
        return null;
    }

}
