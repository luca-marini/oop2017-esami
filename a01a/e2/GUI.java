package a01a.e2;


import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;


public class GUI extends JFrame{
	
	/**
     * 
     */
    private static final long serialVersionUID = 1L;

    public GUI(int size){
	    this.setSize(500, 100);
            this.setDefaultCloseOperation(EXIT_ON_CLOSE);
            this.getContentPane().setLayout(new FlowLayout());
            
            IncrementableArrayList val = new IncrementableArrayListImpl(size);
            List<JButton> buttons = new ArrayList<>();
            
            for(int i = 0; i < size; i++) {
                JButton b = new JButton("0");
                buttons.add(b);
                
                
            
                b.addActionListener(e -> {
                    val.increment(buttons.indexOf(b));
                    b.setText(Integer.toString(val.getValue(buttons.indexOf(b))));
                    
                    if(val.getValue(buttons.indexOf(b)) == size) {
                        b.setEnabled(false);
                    }
                    if(val.isAllEquals()) {
                        this.getContentPane().setVisible(false);
                    }
                    
                }); 
                this.getContentPane().add(b);
            }        
            
            JButton print = new JButton("Print");
            
            print.addActionListener(e -> {
                System.out.println(val.toString());
            });


           
            this.getContentPane().add(print);
            this.setVisible(true);
	}

}
