package a01a.e2;

import java.util.ArrayList;
import java.util.List;

public class IncrementableArrayListImpl implements IncrementableArrayList {
    
    List<Integer> l = new ArrayList<>();

    public IncrementableArrayListImpl(int size) {
        for(int i = 0; i < size ; i++) {
            l.add(0);
        }
    }

    @Override
    public void increment(int i) {
        this.l.set(i, l.get(i) + 1);
    }

    @Override
    public boolean isAllEquals() {
        int val = l.get(0);
        
        for(int i = 0; i < l.size(); i++) {
            if(l.get(i) != val) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
        String s = "<<";
        
        for(int i = 0; i < l.size(); i++) {
                   
            if(i == l.size() - 1) {
                s += l.get(i) + ">>";
            } else {
                s += Integer.toString(l.get(i)) + "|";
            }
        }
        return s;
    }

    @Override
    public int getValue(int i) {
        return l.get(i);
    }

}
