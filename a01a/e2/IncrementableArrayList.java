package a01a.e2;

public interface IncrementableArrayList {
    
    public void increment(int i);
    
    public int getValue(int i);
    
    public boolean isAllEquals();
    
    public String toString();

}
