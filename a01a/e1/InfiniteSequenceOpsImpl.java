package a01a.e1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class InfiniteSequenceOpsImpl implements InfiniteSequenceOps {

    public InfiniteSequenceOpsImpl() {
    }

    @Override
    public <X> InfiniteSequence<X> ofValue(X x) {
        return new InfiniteSequence<X>() {

            @Override
            public X nextElement() {
                return x;
            }
            
        };
    }

    @Override
    public <X> InfiniteSequence<X> ofValues(List<X> l) {
        return new InfiniteSequence<X>() {
            int i = 0;
            @Override
            public X nextElement() {
                
                if(i == l.size() - 1) {
                    i = 0;
                    return l.get(l.size() - 1);
                }
                i++;
                return l.get(i - 1);
            }
            
        };
    }

    @Override
    public InfiniteSequence<Double> averageOnInterval(InfiniteSequence<Double> iseq, int intervalSize) {
        return new InfiniteSequence<Double>() {
            
            List<Double> d = new ArrayList<Double>();

            @Override
            public Double nextElement() {
                 
                d = iseq.nextListOfElements(intervalSize);
                double res = 0;
                
                for(double d : d) {
                   res += d;
                }
                
                return res/d.size();
            }
        };
    }

    @Override
    public <X> InfiniteSequence<X> oneEachInterval(InfiniteSequence<X> iseq, int intervalSize) {
        return new InfiniteSequence<X>() {
            
            List<X> l = new ArrayList<>();
            @Override
            public X nextElement() {
                l = iseq.nextListOfElements(intervalSize);
                
                return l.get(l.size() - 1);
            }
        
        };
    }

    @Override
    public <X> InfiniteSequence<Boolean> equalsTwoByTwo(InfiniteSequence<X> iseq) {
        return new InfiniteSequence<Boolean>() {
            
            @Override
            public Boolean nextElement() {
                return iseq.nextElement() == iseq.nextElement();
            }
        };
    }

    @Override
    public <X, Y extends X> InfiniteSequence<Boolean> equalsOnEachElement(InfiniteSequence<X> isx,
            InfiniteSequence<Y> isy) {
        return new InfiniteSequence<Boolean>() {
            
            @Override
            public Boolean nextElement() {
                return isx.nextElement() == isy.nextElement();
            }
        };
    }

    @Override
    public <X> Iterator<X> toIterator(InfiniteSequence<X> iseq) {
        return new Iterator<X>() {

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public X next() {
                return iseq.nextElement();
            }
        };
    }

}
